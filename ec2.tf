resource "aws_instance" "web-server" {
  ami = var.ami
  instance_type = var.instance-type
  count = var.number_of_ec2
  security_groups = ["${aws_security_group.web-server.name}"]
  user_data = <<-EOF
     #!/bin/bash
     sudo su
     yum update -y
     yum install httpd -y
     systemctl start httpd
     systemctl enable httpd
     echo "<h1>loading from $(hostname -f)..</h1>" > /var/www/html/index.html
  EOF
  tags = {
    Name ="instance_alb-${count.index}"
  }
}

resource "aws_instance" "my-machine" {
  count = var.number_of_ec2 
  ami           = var.ami
  instance_type = var.instance-type
  tags = {
    Name = "my-machine-${count.index}"
  }
}