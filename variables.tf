variable "ami" {
 type        = string
 description = "AMI ID for the EC2 instance"
 default     = "ami-06ca3ca175f37dd66"
 
}
 
variable "instance-type" {
 type        = string
 description = "Instance type for the EC2 instance"
 default     = "t2.micro"
 sensitive   = true
}
 
variable "tags" {
 type = object({
   Name = string
   Env  = string
 })
 
 description = "Tags for the EC2 instance"
 default = {
   Name = "My VM with from default values"
   Env  = "Dev"
 }
}

variable "number_of_ec2" {
 type        = number
 description = "Number Of EC2 instance"
 default     = 2
}